# dnn_xmax_from_ADST

Deep-learning-based reconstruction of the shower maximum Xmax.
Use the code to reconstruct Xmax using the SD data from ADST files.